# ZeNeuSound Survey

First of all, thank you for taking the time to test our sonification software, ZeNeuSound, and providing feedback.
Before going further, please ensure you have downloaded and installed Max/MSP from [cycling74](https://cycling74.com/downloads).

To launch the software, please double click the file ZeNeuSound.maxzip .

The google form is accessible through this links :
https://forms.gle/wH76CN7VPhiLYYb59
